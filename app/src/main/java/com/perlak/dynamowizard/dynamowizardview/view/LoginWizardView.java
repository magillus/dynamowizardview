package com.perlak.dynamowizard.dynamowizardview.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ViewFlipper;

import com.perlak.dynamowizard.dynamowizardview.R;
import com.perlak.dynamowizard.dynamowizardview.dynamos.DynamoManager;

/**
 * Created by mateusz on 6/24/15.
 */
public class LoginWizardView extends ViewFlipper {

    public LoginWizardView(Context context) {
        this(context, null);
    }

    public LoginWizardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        DynamoManager.getInstance().getLoginWizardDynamo(getContext(), "LOGIN_WIZARD").attachView(this);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        DynamoManager.getInstance().getLoginWizardDynamo(getContext(), "LOGIN_WIZARD").detachView(this);
    }

    private void init() {
        // inflate views into flipper
        FrameLayout containerView = (FrameLayout) inflate(getContext(), R.layout.flow_login, null);
        View childView = null;
        while ((childView = containerView.getChildAt(0)) != null) {
            containerView.removeView(childView);
            addView(childView);
        }

    }
}
