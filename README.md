# README #

This is example project to use Dynamo as wizard view for small dynamo instances and flows.

### What is this repository for? ###

Repository is for small project presenting Dynamo library as wizard view.
blog post: http://blog.mateusz.perlak.com/2015/08/wizard-with-dynamo-library

### How do I get set up? ###

Git clone project and build.

### Who do I talk to? ###

Mateusz