package com.perlak.dynamowizard.dynamowizardview.dynamos;

import couk.doridori.dynamo.Dynamo;
import couk.doridori.dynamo.StateMachine;

/**
 * Created by mateusz on 6/26/15.
 */
public abstract class BaseDynamo<T extends StateMachine.State> extends Dynamo<T> {
    protected boolean activated;

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public boolean goBack() {

        T state = getStateMachine().getCurrentState();
        if (state instanceof ReversableState) {
            T backStep = (T) ((ReversableState) state).getBackStep();
            if (backStep != null) {
                newState(backStep);
                return true;
            }
        }
        return false;
    }

}
