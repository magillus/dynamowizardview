package com.perlak.dynamowizard.dynamowizardview.dynamos;

import android.content.Context;

import couk.doridori.dynamo.DynamoHolder;

/**
 * Singleton. Handles Dynamo instances.
 * <p/>
 * If using Dagger you may want to inject this class as a @Singleton instead
 */
public class DynamoManager {
    //======================================================================================
    // Singleton
    //======================================================================================

    public static synchronized DynamoManager getInstance() {
        return sInstance;
    }

    private static DynamoManager sInstance = new DynamoManager();
    public DynamoHolder<LoginWizardDynamo> mComputationDynamoHolder = new DynamoHolder<LoginWizardDynamo>(1);


    public LoginWizardDynamo getLoginWizardDynamo(final Context context, String meta) {
        return mComputationDynamoHolder.getDynamo(meta, new DynamoHolder.DynamoFactory<LoginWizardDynamo>() {
            @Override
            public LoginWizardDynamo buildDynamo() {
                return new LoginWizardDynamo(context);
            }
        });
    }

    /**
     * checks which dynamo is active and process back button.
     */
    public boolean executeOnBackPressed() {
        for (BaseDynamo dynamo : mComputationDynamoHolder.getAll().values()) {
            if (dynamo.isActivated()) {
                if (dynamo.goBack()) {
                    return true;
                }
            }
        }
        return false;
    }
}
