package com.perlak.dynamowizard.dynamowizardview.dynamos;

import couk.doridori.dynamo.StateMachine;

/**
 * Created by mateusz on 6/26/15.
 */
public abstract class ReversableState extends StateMachine.State {

    abstract StateMachine.State getBackStep();
}
