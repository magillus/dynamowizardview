package com.perlak.dynamowizard.dynamowizardview.dynamos;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.perlak.dynamowizard.dynamowizardview.R;
import com.perlak.dynamowizard.dynamowizardview.service.LoginService;
import com.perlak.dynamowizard.dynamowizardview.service.model.User;
import com.perlak.dynamowizard.dynamowizardview.view.LoginWizardView;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import couk.doridori.dynamo.StateMachine;

/**
 * Created by mateusz on 6/26/15.
 */
public class LoginWizardDynamo extends BaseDynamo<LoginWizardDynamo.LoginBaseState> {
    // inject that
    private final LoginService loginService;
    private LoginWizardView loginWizardView;
    private User model = new User(null, null, null);

    User getModel() {
        return model;
    }

    private View getCurrentView() {
        return loginWizardView.getChildAt(loginWizardView.getDisplayedChild());
    }

    public Button getActionButton() {
        return (Button) getCurrentView().findViewById(R.id.login_action);
    }

    public EditText getUsernameInput() {
        return (EditText) getCurrentView().findViewById(R.id.login_username_input);
    }

    public EditText getPasswordInput() {
        return (EditText) getCurrentView().findViewById(R.id.login_password_input);
    }

    public LoginWizardDynamo(Context context) {
        loginService = new LoginService(context);
    }

    public void attachView(LoginWizardView loginWizardView) {
        this.loginWizardView = loginWizardView;
        setActivated(true);
        newState(new CheckinLoginState());
    }

    public void detachView(LoginWizardView loginWizardView) {
        if (this.loginWizardView != loginWizardView) {
            this.loginWizardView = null;
            setActivated(false);
        }
    }

    public View findViewById(int resId) {
        return getCurrentView().findViewById(resId);
    }

    private void hideKeyboard(View v) {
        //hide keyboard
        InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public class LoggedInState extends LoginBaseState {
        @Override
        StateMachine.State getBackStep() {
            return null;
        }

        @Override
        public int getViewIndex() {
            return 2;
        }

        @Override
        public void enteringState() {
            super.enteringState();
            ((TextView) findViewById(R.id.label)).setText(String.format("You are logged in %s, do you want to log out?", getModel().username));
            bindListener(getActionButton(), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    loginService.logout();
                    getModel().username = null;
                    getModel().password = null;
                    getModel().email = null;
                    newState(new CheckinLoginState());
                }
            });
        }
    }

    public class CheckinLoginState extends LoginProgressBaseState {

        @Override
        protected LoginBaseState getBackStep() {
            return null;
        }

        @Override
        public void enteringState() {
            super.enteringState();
            if (loginService.isLoggedIn()) {
                newState(new LoggedInState());
            } else {
                newState(new LoginState());
            }
        }
    }

    public class ForgotPasswordProcessState extends LoginProgressBaseState {

        @Override
        protected LoginBaseState getBackStep() {
            return new CheckinLoginState();
        }

        @Override
        public void enteringState() {
            super.enteringState();
            loginService.resetPassword(getModel().username, new LoginService.ResetPasswordCallback() {
                @Override
                public void onPassowordReset(boolean success) {
                    if (success) {
                        newState(new LoginState());
                    } else {
                        newState(new ForgotPasswordEntryState());
                    }
                }
            });
        }
    }

    public class ForgotPasswordEntryState extends LoginBaseState {

        @Override
        protected LoginBaseState getBackStep() {
            return new LoginState();
        }

        @Override
        public int getViewIndex() {
            return 4;
        }

        @Override
        public void enteringState() {
            super.enteringState();
            getUsernameInput().setText(getModel().username);
            getActionButton().setImeActionLabel("Reset", EditorInfo.IME_ACTION_GO);

            bindListener(getUsernameInput(), new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    boolean handled = false;
                    if (actionId == EditorInfo.IME_ACTION_GO) {
                        reset();
                        handled = true;
                    }
                    return handled;
                }
            });
            bindListener(getActionButton(), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    reset();
                }
            });
        }

        private void reset() {
            getModel().username = getUsernameInput().getText().toString();
            getModel().password = null;
            hideKeyboard(getUsernameInput());
            newState(new ForgotPasswordProcessState());
        }
    }

    public class SuccessLoginState extends LoginBaseState {

        @Override
        protected LoginBaseState getBackStep() {
            return null;
        }

        @Override
        public int getViewIndex() {
            return 3;
        }

        @Override
        public void enteringState() {
            super.enteringState();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    newState(new CheckinLoginState());
                }
            }, 5000);
        }
    }

    public class LogingInState extends LoginProgressBaseState {

        @Override
        protected LoginBaseState getBackStep() {
            return new LoginState();
        }

        @Override
        public void enteringState() {
            super.enteringState();
            // call out loging
            loginService.login(getModel().username, getModel().password, new LoginService.LoginCallback() {
                @Override
                public void onLogin(boolean success) {
                    if (success) {
                        newState(new SuccessLoginState());
                    } else {
                        newState(new LoginState());
                    }
                }
            });
        }
    }

    public class RegisteringState extends LoginProgressBaseState {

        @Override
        StateMachine.State getBackStep() {
            return new RegisterState();
        }

        @Override
        public void enteringState() {
            super.enteringState();
            register();
        }

        private void register() {
            loginService.registerUser(getModel().username,
                    getModel().password,
                    getModel().email,
                    new LoginService.RegisterCallback() {
                        @Override
                        public void onUserRegistered(User user) {
                            if (user != null) {
                                newState(new LoggedInState());
                            } else {
                                newState(new CheckinLoginState());
                            }
                        }
                    }
            );
        }
    }

    public class RegisterState extends LoginBaseState {

        @Override
        public int getViewIndex() {
            return 5;
        }

        @Override
        StateMachine.State getBackStep() {
            return new CheckinLoginState();
        }

        @Override
        public void enteringState() {
            super.enteringState();
            bindListener(getActionButton(), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    storeRegister();
                }
            });
            getPasswordInput().setImeActionLabel("REGSITER", EditorInfo.IME_ACTION_GO);
            bindListener(getPasswordInput(), new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    boolean handled = false;
                    if (actionId == EditorInfo.IME_ACTION_GO) {
                        storeRegister();
                        handled = true;
                    }
                    return handled;
                }
            });
        }

        private void storeRegister() {
            getModel().username = getUsernameInput().getText().toString();
            getModel().password = getPasswordInput().getText().toString();
            getModel().email = ((EditText) findViewById(R.id.login_email_input)).getText().toString();
            newState(new RegisteringState());
        }
    }

    public class LoginState extends LoginBaseState {
        @Override
        @Nullable
        protected LoginBaseState getBackStep() {
            return null;
        }

        @Override
        public int getViewIndex() {
            return 1;
        }


        @Override
        public void enteringState() {
            super.enteringState();
            getUsernameInput().setText(getModel().username);
            getPasswordInput().setText(getModel().password);
            final View.OnClickListener loginListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getModel().username = getUsernameInput().getText().toString();
                    getModel().password = getPasswordInput().getText().toString();
                    newState(new LogingInState());
                }
            };
            getPasswordInput().setImeActionLabel("Login", EditorInfo.IME_ACTION_GO);
            bindListener(getPasswordInput(), new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    boolean handled = false;
                    if (actionId == EditorInfo.IME_ACTION_GO) {
                        hideKeyboard(v);
                        loginListener.onClick(v);
                        handled = true;
                    }
                    return handled;
                }
            });
            bindListener(getActionButton(), loginListener);
            bindListener(findViewById(R.id.login_forgot_password), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    hideKeyboard(v);
                    newState(new ForgotPasswordEntryState());
                }
            });
            bindListener(findViewById(R.id.login_register), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    newState(new RegisterState());
                }
            });
        }
    }

    public abstract class LoginProgressBaseState extends LoginBaseState {
        @Override
        public int getViewIndex() {
            return 0;//loading icon
        }
    }

    public abstract class LoginBaseState extends ReversableState implements IndexableView {

        private Map<View, Method> bindMap = new HashMap<>();

        @Override
        public void enteringState() {
            super.enteringState();
            loginWizardView.setDisplayedChild(getViewIndex());
        }

        @Override
        public void exitingState() {
            super.exitingState();
            unbind();
        }

        protected void unbind() {
            for (Map.Entry<View, Method> bindEntry : bindMap.entrySet()) {
                Object nullListener = null;
                try {
                    bindEntry.getValue().invoke(bindEntry.getKey(), nullListener);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
            bindMap.clear();
        }

        protected void bindListener(View v, Object listener) {
            for (Method method : v.getClass().getMethods()) {
                if (method.getParameterTypes().length == 1) {//must be one argument
                    // check matching type
                    boolean foundMatch = false;

                    if (method.getParameterTypes()[0].equals(listener.getClass())) {
                        foundMatch = true;
                    } else {
                        for (Class interfaces : listener.getClass().getInterfaces()) {
                            if (interfaces.equals(method.getParameterTypes()[0])) {
                                foundMatch = true;
                                break;
                            }
                        }
                    }

                    if (foundMatch) {
                        try {
                            method.invoke(v, listener);
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        } catch (InvocationTargetException e) {
                            e.printStackTrace();
                        }
                        bindMap.put(v, method);
                    }
                }
            }
        }
    }

    public interface IndexableView {
        int getViewIndex();
    }
}
