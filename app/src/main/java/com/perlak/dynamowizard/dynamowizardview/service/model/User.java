package com.perlak.dynamowizard.dynamowizardview.service.model;

import com.bluelinelabs.logansquare.annotation.JsonObject;

/**
 * Created by mateusz on 6/26/15.
 */
@JsonObject(fieldDetectionPolicy = JsonObject.FieldDetectionPolicy.NONPRIVATE_FIELDS)
public class User {
    public String username;
    public String password;
    public String email;

    public User() {

    }
    public User(String username, String password, String email) {
        this.username = username;
        this.password = password;
        this.email = email;
    }
}
