package com.perlak.dynamowizard.dynamowizardview.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;

import com.bluelinelabs.logansquare.LoganSquare;
import com.perlak.dynamowizard.dynamowizardview.service.model.User;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Fake login service
 * Created by mateusz on 6/24/15.
 */
public class LoginService {
    private static final String CURRENT_USER = "CURRENT_USER";
    private static final String USERS = "USERS";
    private SharedPreferences storage;

    public boolean isLoggedIn() {
        return getCurrentUser() != null;
    }

    public List<User> getUsers() {
        String users = storage.getString(USERS, null);
        if (users != null) {
            try {
                return LoganSquare.parseList(users, User.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return Collections.emptyList();
    }

    public User getCurrentUser() {
        String userJson = storage.getString(CURRENT_USER, null);
        if (userJson != null) {
            try {
                User user = LoganSquare.parse(userJson, User.class);
                return user;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public void setCurrentUser(User user) {
        if (user != null) {
            try {
                storage.edit().putString(CURRENT_USER, LoganSquare.serialize(user)).apply();
                return;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        storage.edit().putString(CURRENT_USER, null).apply();
    }

    public LoginService(Context context) {
        storage = context.getSharedPreferences("LOGIN_SERVICE", Context.MODE_PRIVATE);
    }

    public void addUser(User user) {
        List<User> users = new ArrayList<>(getUsers());
        users.add(user);
        try {
            storage.edit().putString(USERS, LoganSquare.serialize(users, User.class)).apply();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void login(final String username, final String password, final LoginCallback callback) {
        //simulate login
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                for (User user : getUsers()) {
                    if (username.equals(user.username) && password.equals(user.password)) {
                        setCurrentUser(new User(username, password, null));
                    }
                    callback.onLogin(isLoggedIn());
                }
            }
        }, 2000);
    }

    public void resetPassword(String username, final ResetPasswordCallback callback) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                callback.onPassowordReset(true);
            }
        }, 1000);
    }

    public void registerUser(final String username, final String password, final String email, final RegisterCallback callback) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                User user = new User(username, password, email);
                addUser(user);
                setCurrentUser(user);
                if (callback != null) {
                    callback.onUserRegistered(user);
                }
            }
        }, 2000);
    }

    public void logout() {
        setCurrentUser(null);
    }


    public interface LoginCallback {
        void onLogin(boolean success);
    }

    public interface ResetPasswordCallback {
        void onPassowordReset(boolean success);
    }

    public interface RegisterCallback {
        void onUserRegistered(User user);
    }
}
