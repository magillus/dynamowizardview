package com.perlak.dynamowizard.dynamowizardview;

import android.support.v7.app.AppCompatActivity;

import com.perlak.dynamowizard.dynamowizardview.dynamos.DynamoManager;

/**
 * Created by mateusz on 6/26/15.
 */
public class BaseActivity extends AppCompatActivity {

    @Override
    public void onBackPressed() {
        if (!DynamoManager.getInstance().executeOnBackPressed()) {
            super.onBackPressed();
        }
    }
}
